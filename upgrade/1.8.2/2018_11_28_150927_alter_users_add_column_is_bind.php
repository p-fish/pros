<?php

namespace We7\V182;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1543388967
 * @version 1.8.2
 */

class AlterUsersAddColumnIsBind {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('users', array('is_bind'))) {
			$table_name = tablename('users');
			$sql = <<<EOT
				ALTER TABLE {$table_name} ADD `is_bind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '第三方登录是否注册绑定' ;
EOT;
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		