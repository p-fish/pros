<?php

namespace We7\V279;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1631607305
 * @version 2.7.9
 */

class UpdateModulesTable {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules', 'createtime')) {
			pdo_query("ALTER TABLE " . tablename('modules') . " ADD `createtime` INT(10) UNSIGNED NOT NULL;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
