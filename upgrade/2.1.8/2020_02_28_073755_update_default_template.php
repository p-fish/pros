<?php

namespace We7\V218;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1582846675
* @version 2.1.8
*/

class UpdateDefaultTemplate {

/**
 *  执行更新
 */
public function up() {
	table('modules')->where(array('name' => 'default', 'application_type' => 2))->fill(array('issystem' => 1))->save();
}

/**
 *  回滚更新
 */
public function down() {


}
}
