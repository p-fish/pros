<?php

namespace We7\V2711;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1641458677
* @version 2.7.11
*/

class UpdateModulesCloud {
	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('modules_cloud', 'show_in_tcb')) {
			pdo_run("ALTER TABLE " . tablename('modules_cloud') . " ADD COLUMN `show_in_tcb` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否显示在云安装列表:1是0否'");
		}
		if (pdo_fieldexists('modules_cloud', 'modified_time')) {
			pdo_run("ALTER TABLE " . tablename('modules_cloud') . " ADD `modified_time` INT(10) UNSIGNED NOT NULL COMMENT '应用更新时间' AFTER `is_ban`;");
		}
		if (pdo_fieldexists('modules_cloud', 'tcb_version')) {
			pdo_run("ALTER TABLE " . tablename('modules_cloud') . " ADD `tcb_version` VARCHAR(8) NOT NULL COMMENT 'TCB已准备好的应用代码版本' AFTER `version`;");
		}
		if (pdo_fieldexists('modules_cloud', 'tcb_status')) {
			pdo_run("ALTER TABLE " . tablename('modules_cloud') . " ADD `tcb_status` TINYINT(2) NOT NULL COMMENT 'TCB代码准备状态：0未准备；10正在上传；20准备完成待部署；50代码已放在站点内' AFTER `tcb_version`;");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
