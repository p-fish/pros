<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1550818583
 * @version 1.8.8
 */

class AlterImsUsersLastuseAddIndexModulename {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_indexexists('users_lastuse', 'modulename')){
			pdo_query("ALTER TABLE " .tablename('users_lastuse') ." ADD INDEX modulename (`modulename`)");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		