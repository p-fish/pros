<?php

namespace We7\V201;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1552556483
 * @version 1.8.8
 */

class UpgradeModulesBindings {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules_bindings','multilevel')) {
			pdo_query("ALTER TABLE " . tablename('modules_bindings') . " ADD `multilevel` TINYINT(1) NOT NULL COMMENT '是否一级菜单'");
		}
		if (!pdo_fieldexists('modules_bindings','parent')) {
			pdo_query("ALTER TABLE " . tablename('modules_bindings') . " ADD `parent` VARCHAR(50) NOT NULL COMMENT '父级菜单name'");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		