<?php

namespace We7\V184;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1542106057
 * @version 1.8.4
 */

class MigrateDataToUniModules {

	/**
	 *  执行更新
	 */
	public function up() {
		load()->model('module');
		load()->model('cache');
		$truncate_sql = "truncate table " . tablename('uni_modules');
		pdo_query($truncate_sql);
		$accounta_all = pdo_fetchall("select u.uniacid from " . tablename('uni_account') . " u left join " . tablename('account') . " a on u.uniacid=a.uniacid where a.isdeleted=0 and a.type not in(4,6,11,7)");
		$module_system = module_system();
		foreach ($accounta_all as $account_val) {
			$uniacid = $account_val['uniacid'];
			$uni_modules = uni_modules_by_uniacid($uniacid);

			if (!empty($uni_modules)) {
				foreach ($uni_modules as $uni_module_key => $uni_module_val) {
					if (in_array($uni_module_key, $module_system)) {
						continue;
					}
					if (!empty($uni_module_val['main_module'])) {
						continue;
					}
					$account_modules_data = array('uniacid' => $uniacid, 'module_name' => $uni_module_val['name']);
					$account_modules_exists = pdo_get('uni_modules', $account_modules_data);
					if (empty($account_modules_exists)) {
						pdo_insert('uni_modules', $account_modules_data);
					}
				}
				cache_clean(cache_system_key('unimodules'));
				cache_clean(cache_system_key('user_modules'));
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		