<?php

namespace We7\V272;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1609242475
 * @version 2.7.2
 */

class InsertModuleTagField {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules', 'label')) {
			pdo_query('ALTER TABLE '.tablename('modules')." ADD COLUMN `label` VARCHAR(500) NOT NULL DEFAULT '';");
		}
		if (!pdo_fieldexists('modules_cloud', 'label')) {
			pdo_query('ALTER TABLE '.tablename('modules_cloud')." ADD COLUMN `label` VARCHAR(500) NOT NULL DEFAULT '';");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
