<?php

namespace We7\V212;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1575617653
* @version 2.1.2
*/

class UpdateSettingLogin {

	/**
	 *  执行更新
	 */
	public function up() {
		global $_W;
		$settings = $_W['setting']['basic'];
		if (empty($settings['login_template'])) {
			$settings['login_template'] = 'base';
			setting_save($settings, 'basic');
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
