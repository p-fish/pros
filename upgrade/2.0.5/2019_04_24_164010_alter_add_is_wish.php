<?php

namespace We7\V205;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1556095210
 * @version 2.0.5
 */

class AlterAddIsWish {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('site_store_goods','is_wish')) {
			pdo_query("ALTER TABLE " . tablename('site_store_goods') . " ADD `is_wish` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为星愿应用 1 是 0 否';");
		}
		if (!pdo_fieldexists('site_store_goods','logo')) {
			pdo_query("ALTER TABLE " . tablename('site_store_goods') . " ADD `logo` varchar(300) NOT NULL DEFAULT '';");
		}

		if (!pdo_fieldexists('site_store_order','is_wish')) {
			pdo_query("ALTER TABLE " . tablename('site_store_order') . " ADD `is_wish` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为星愿应用 1 是 0 否';");
		}

		if (!pdo_fieldexists('core_paylog','is_wish')) {
			pdo_query("ALTER TABLE " . tablename('core_paylog') . " ADD `is_wish` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为星愿应用 1 是 0 否';");
		}

		if (!pdo_fieldexists('core_refundlog','is_wish')) {
			pdo_query("ALTER TABLE " . tablename('core_refundlog') . " ADD `is_wish` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为星愿应用 1 是 0 否';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		