<?php

namespace We7\V204;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1555556058
 * @version 2.0.4
 */

class CreateTableMcFansTag {

	/**
	 *  执行更新
	 */
	public function up()
	{
		if (!pdo_tableexists('mc_fans_tag')) {
			$table_name = tablename('mc_fans_tag');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`uniacid` int(11) DEFAULT '0' COMMENT '公众号id',
	`fanid` int(11) NOT NULL DEFAULT '0' COMMENT '粉丝id',
	`openid` varchar(50) NOT NULL COMMENT '用户的标识',
	`subscribe` int(11) DEFAULT '0' COMMENT '用户是否订阅该公众号标识',
	`nickname` varchar(100) DEFAULT NULL COMMENT '用户的昵称',
	`sex` int(11) DEFAULT '0' COMMENT '用户的性别 值为1时是男性 值为2时是女性 值为0时是未知',
	`language` varchar(50) DEFAULT NULL COMMENT '用户的语言',
	`city` varchar(50) DEFAULT NULL COMMENT '用户所在城市',
	`province` varchar(50) DEFAULT NULL COMMENT '用户所在省份',
	`country` varchar(50) DEFAULT NULL COMMENT '用户所在国家',
	`headimgurl` varchar(150) DEFAULT NULL COMMENT '用户头像',
	`subscribe_time` int(11) NOT NULL DEFAULT '0' COMMENT '用户关注时间',
	`unionid` varchar(100) DEFAULT NULL COMMENT '只有在用户将公众号绑定到微信开放平台帐号',
	`remark` varchar(250) DEFAULT NULL COMMENT '公众号运营者对粉丝的备注公众号运营者可在微信公众平台用户管理界面对粉丝添加备注',
	`groupid` varchar(100) DEFAULT NULL COMMENT '用户所在的分组id',
	`tagid_list` varchar(250) DEFAULT NULL COMMENT '用户被打上的标签id列表',
	`subscribe_scene` varchar(100) DEFAULT NULL COMMENT '返回用户关注的渠道来源',
	`qr_scene_str` varchar(250) DEFAULT NULL COMMENT '二维码扫码场景描述',
	`qr_scene` varchar(250) DEFAULT NULL COMMENT '二维码扫码场景',
	PRIMARY KEY (`id`),
  KEY `fanid` (`fanid`),
  KEY `openid` (`openid`)
) DEFAULT CHARSET=utf8;
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		