<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1551931993
 * @version 1.8.8
 */

class DeleteClerkInfoWithWrongPermissions {

	/**
	 *  执行更新
	 */
	public function up() {
		$user_accounts = pdo_getall('uni_account_users', array('role' => 'clerk'));
		if (!empty($user_accounts)) {
			foreach ($user_accounts as $user_account) {
				$data = array('uid' => $user_account['uid'], 'uniacid' => $user_account['uniacid']);
				$user_permission = pdo_get('users_permission', $data);
				if (!$user_permission) {
					pdo_delete('uni_account_users', $data);
					pdo_delete('users_lastuse', $data);
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		