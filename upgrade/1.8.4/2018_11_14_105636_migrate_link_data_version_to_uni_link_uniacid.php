<?php

namespace We7\V184;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1542164196
 * @version 1.8.4
 */

class MigrateLinkDataVersionToUniLinkUniacid {

	/**
	 *  执行更新
	 */
	public function up() {
		$phoneapp_data = pdo_getall('phoneapp_versions', array('modules <>' => ''), array('id', 'uniacid', 'modules'));
		$this->insertUniLinkUniacid($phoneapp_data, 'phoneapp_versions');

		$wxapp_data = pdo_getall('wxapp_versions', array('modules <>' => ''), array('id', 'uniacid', 'modules'));
		$this->insertUniLinkUniacid($wxapp_data, 'wxapp_versions');
	}

	public function insertUniLinkUniacid($data, $source_table) {
		if (!empty($data)) {
			foreach ($data as $item) {
				$modules = iunserializer($item['modules']);
				if (empty($modules) || !is_array($modules)) {
					continue;
				}
				$has_link_uniacid = false;
				foreach ($modules as &$module) {
					if (empty($module['name'])) {
						continue;
					}
					$module['uniacid'] = intval($module['uniacid']);
					if (!empty($module['uniacid'])) {
						pdo_insert('uni_link_uniacid', array(
							'version_id' => $item['id'],
							'uniacid' => $item['uniacid'],
							'module_name' => $module['name'],
							'link_uniacid' => $module['uniacid'],
						));
						unset($module['uniacid']);
						$has_link_uniacid = true;
					}
				}
				if ($has_link_uniacid) {
					pdo_update($source_table, array('modules' => iserializer($modules)), array('id' => $item['id']));
				}
			}
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
