<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1553494072
 * @version 1.8.8
 */

class CheckUserMobileBindInfo {

	/**
	 *  执行更新
	 */
	public function up() {
		$mobile_binds = pdo_fetchall("SELECT * FROM " . tablename('users_bind') . " WHERE third_type = " . USER_REGISTER_TYPE_MOBILE);
		if (!empty($mobile_binds)) {
			foreach ($mobile_binds as $bind_info) {
				$is_mobile = preg_match(REGULAR_MOBILE, $bind_info['third_nickname']);
				if (!$is_mobile) {
					pdo_delete('users_bind', array('id' => $bind_info['id']));
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		