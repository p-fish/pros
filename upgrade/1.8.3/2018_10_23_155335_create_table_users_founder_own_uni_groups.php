<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540281215
 * @version 1.8.3
 */

class CreateTableUsersFounderOwnUniGroups {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_exists('users_founder_own_uni_groups')) {
			$table_name = tablename('users_founder_own_uni_groups');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(10) unsigned not null AUTO_INCREMENT,
	`founder_uid` int(10) unsigned not null COMMENT '副创始人uid',
	`uni_group_id` int(10) unsigned not null COMMENT '应用权限组id',
	PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8 COMMENT '副创始人应用权限组表';
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		