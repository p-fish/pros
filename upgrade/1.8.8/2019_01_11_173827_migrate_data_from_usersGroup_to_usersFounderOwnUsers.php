<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540633107
 * @version 1.8.8
 */

class MigrateDataFromUsersGroupToUsersFounderOwnUsers {

	/**
	 *  执行更新
	 */
	public function up() {
		$founder_own_users_groups = tablename('users_founder_own_users_groups');
		$users_group = tablename('users_group');
		$sql = <<<EOF
INSERT INTO $founder_own_users_groups(`founder_uid`, `users_group_id`) select `owner_uid`, `id` from $users_group where `owner_uid` != 0;
EOF;
		pdo_query($sql);
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		