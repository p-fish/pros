<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540276601
 * @version 1.8.3
 */

class CreateTableUsersFounderOwnUsers {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_exists('users_founder_own_users')) {
			$table_name = tablename('users_founder_own_users');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(10) unsigned not null AUTO_INCREMENT,
	`uid` int(10) unsigned not null COMMENT '用户uid',
	`founder_uid` int(10) unsigned not null COMMENT '副创始人uid',
	PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8 COMMENT '副创始人用户表';
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		