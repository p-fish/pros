<?php

namespace We7\V2756;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1652255774
* @version 2.7.56
*/

class UpdateCoreAttachment {

/**
 *  执行更新
 */
	public function up() {
		if (!pdo_fieldexists('core_attachment', 'tcb_file_id')) {
			pdo_query("ALTER TABLE " . tablename('core_attachment') . " ADD `tcb_file_id` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'TCB上传返回的file_id，用于删除';");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
