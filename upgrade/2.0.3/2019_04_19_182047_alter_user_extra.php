<?php

namespace We7\V203;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1555669247
 * @version 2.0.3
 */

class AlterUserExtra {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_indexexists('users_extra_group', 'uid')){
			pdo_query("ALTER TABLE " .tablename('users_extra_group') ." ADD INDEX uid (`uid`)");
		}
		if(!pdo_indexexists('users_extra_group', 'uni_group_id')){
			pdo_query("ALTER TABLE " .tablename('users_extra_group') . " ADD INDEX uni_group_id (`uni_group_id`)");
		}
		if(!pdo_indexexists('users_extra_group', 'create_group_id')){
			pdo_query("ALTER TABLE " .tablename('users_extra_group') ." ADD INDEX create_group_id (`create_group_id`)");
		}


		if(!pdo_indexexists('users_extra_limit', 'uid')){
			pdo_query("ALTER TABLE " .tablename('users_extra_limit') . " ADD INDEX uid (`uid`)");
		}

		if(!pdo_indexexists('users_extra_modules', 'uid')){
			pdo_query("ALTER TABLE " .tablename('users_extra_modules') . " ADD INDEX uid (`uid`)");
		}
		if(!pdo_indexexists('users_extra_modules', 'module_name')){
			pdo_query("ALTER TABLE " .tablename('users_extra_modules') . " ADD INDEX module_name (`module_name`)");
		}

		if(!pdo_indexexists('users_extra_templates', 'uid')){
			pdo_query("ALTER TABLE " .tablename('users_extra_templates') . " ADD INDEX uid (`uid`)");
		}
		if(!pdo_indexexists('users_extra_templates', 'template_id')){
			pdo_query("ALTER TABLE " .tablename('users_extra_templates') . " ADD INDEX template_id (`template_id`)");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		