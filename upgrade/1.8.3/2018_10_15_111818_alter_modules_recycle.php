<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1539573498
 * @version 1.8.3
 */

class AlterModulesRecycle {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('modules_recycle', 'account_support') && !pdo_fieldexists('modules_recycle', 'aliapp_support')) {
			pdo_run("ALTER TABLE " . tablename('modules_recycle') . "ADD (
				`account_support` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '默认0,为1时停用或删除该支持有效',
				`wxapp_support` TINYINT(1) NOT NULL DEFAULT 0,
				`welcome_support` TINYINT(1) NOT NULL DEFAULT 0,
				`webapp_support` TINYINT(1) NOT NULL DEFAULT 0,
				`phoneapp_support` TINYINT(1) NOT NULL DEFAULT 0,
				`xzapp_support` TINYINT(1) NOT NULL DEFAULT 0,
				`aliapp_support` TINYINT(1) NOT NULL DEFAULT 0
			)");
		}
		if(!pdo_fieldexists('modules_recycle', 'baiduapp_support')) {
			pdo_query("ALTER TABLE " . tablename('modules_recycle') . " ADD `baiduapp_support` tinyint(1) NOT NULL DEFAULT 0;");
		}
		if(!pdo_fieldexists('modules_recycle', 'toutiaoapp_support')) {
			pdo_query("ALTER TABLE " . tablename('modules_recycle') . " ADD `toutiaoapp_support` tinyint(1) NOT NULL DEFAULT 0;");
		}
		if(!pdo_fieldexists('modules', 'baiduapp_support')) {
			pdo_query("ALTER TABLE " . tablename('modules') . " ADD `baiduapp_support` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否支持百度小程序';");
		}
		if(!pdo_fieldexists('modules', 'toutiaoapp_support')) {
			pdo_query("ALTER TABLE " . tablename('modules') . " ADD `toutiaoapp_support` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否支持头条小程序';");
		}

		$fields = array('account_support', 'wxapp_support', 'welcome_support', 'webapp_support', 'phoneapp_support', 'xzapp_support', 'aliapp_support', 'baiduapp_support', 'toutiaoapp_support');

		$delete_modules = pdo_getall('modules_recycle', array('type' => 2), array_merge($fields, array('name')), 'name');
		if (!empty($delete_modules)) {
			foreach ($delete_modules as $name => $delete) {
				foreach ($fields as $field) {
					if ($delete[$field] == 1) {
						unset($delete_modules[$name]);
						break;
					}
				}
			}
			if (!empty($delete_modules)) {
				$count_modules_cloud = pdo_getcolumn('modules_cloud', array(), 'count(*)');
				if ($count_modules_cloud < count($delete_modules)) {
					load()->model('module');
					module_upgrade_info();
				}
				$cloud_modules = pdo_getall('modules_cloud', array('name' => array_keys($delete_modules)), array_merge($fields, array('name')), 'name');
				if (!empty($cloud_modules)) {
					foreach ($cloud_modules as $module) {
						if (!empty($module['name'])) {
							pdo_update('modules_recycle', array(
								'account_support' => $module['account_support'] == 2 ? 1: 0,
								'wxapp_support' => $module['wxapp_support'] == 2 ? 1: 0,
								'welcome_support' => $module['welcome_support'] == 2 ? 1: 0,
								'webapp_support' => $module['webapp_support'] == 2 ? 1: 0,
								'phoneapp_support' => $module['phoneapp_support'] == 2 ? 1: 0,
								'xzapp_support' => $module['xzapp_support'] == 2 ? 1: 0,
								'aliapp_support' => $module['aliapp_support'] == 2 ? 1: 0,
								'baiduapp_support' => $module['baiduapp_support'] == 2 ? 1: 0,
								'toutiaoapp_support' => $module['toutiaoapp_support'] == 2 ? 1: 0
							), array(
								'name' => $module['name'],
								'type' => 2
							));
						}
					}
				}
			}
		}

		$recycle_modules = pdo_getall('modules_recycle', array('type' => 1), array_merge($fields, array('name')), 'name');
		if (!empty($recycle_modules)) {
			foreach ($recycle_modules as $name => $recycle) {
				foreach ($fields as $field) {
					if ($recycle[$field] == 1) {
						unset($recycle_modules[$name]);
						break;
					}
				}
			}
			if (!empty($recycle_modules)) {
				$modules = pdo_getall('modules', array('name' => array_keys($recycle_modules)), array_merge($fields, array('name')), 'name');
				if (!empty($modules)) {
					foreach ($modules as $module) {
						if (!empty($module['name'])) {
							pdo_update('modules_recycle', array(
								'account_support' => $module['account_support'] == 2 ? 1 : 0,
								'wxapp_support' => $module['wxapp_support'] == 2 ? 1 : 0,
								'welcome_support' => $module['welcome_support'] == 2 ? 1 : 0,
								'webapp_support' => $module['webapp_support'] == 2 ? 1 : 0,
								'phoneapp_support' => $module['phoneapp_support'] == 2 ? 1 : 0,
								'xzapp_support' => $module['xzapp_support'] == 2 ? 1 : 0,
								'aliapp_support' => $module['aliapp_support'] == 2 ? 1 : 0,
								'baiduapp_support' => $module['baiduapp_support'] == 2 ? 1 : 0,
								'toutiaoapp_support' => $module['toutiaoapp_support'] == 2 ? 1 : 0
							), array(
								'name' => $module['name'],
								'type' => 1
							));
						}
					}
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		