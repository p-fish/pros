<?php

namespace We7\V2710;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1635243605
 * @version 2.7.10
 */

class UpdateMcMassRecord {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('mc_mass_record', 'status_msg')) {
			pdo_query("ALTER TABLE " . tablename('mc_mass_record') . " ADD `status_msg` VARCHAR( 64 ) NOT NULL AFTER `status`;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
