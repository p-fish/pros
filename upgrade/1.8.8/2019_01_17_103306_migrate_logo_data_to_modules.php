<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1547692386
 * @version 1.8.8
 */

class MigrateLogoDataToModules {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules', 'logo')) {
			pdo_query("ALTER TABLE " . tablename('modules') . " ADD `logo` varchar(250) NOT NULL DEFAULT '' ;");
		}
		$modules = pdo_fetchall("SELECT name FROM " . tablename('modules') . " WHERE issystem!=1");
		foreach ($modules as $key => $val) {
			if (file_exists (IA_ROOT . '/addons/' . $val['name'] . '/icon-custom.jpg')) {
				$val['logo'] = 'addons/' . $val['name'] . '/icon-custom.jpg';
			} else {
				$val['logo'] = 'addons/' . $val['name'] . '/icon.jpg';
			}
			pdo_update('modules', array('logo' => $val['logo']), array('name' => $val['name']));
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		