<?php

namespace We7\V211;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1574325969
* @version 2.1.1
*/

class InitHelpMenu {

/**
 *  执行更新
 */
public function up() {
	$menu_array = array('help', 'custom_help');
	foreach ($menu_array as $permission_name) {
		$menu_db = pdo_get('core_menu', array('permission_name' => $permission_name));

		if (!empty($menu_db)) {
			pdo_update('core_menu', array('is_display' => 0), array('permission_name' => $permission_name));
		} else {
			$menu_data = array('is_display' => 0, 'permission_name' => $permission_name);
			$menu_data['is_system'] = 1;
			$menu_data['group_name'] = 'frame';
			pdo_insert('core_menu', $menu_data);
		}
	}
}

/**
 *  回滚更新
 */
public function down() {


}
}
