<?php

namespace We7\V217;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1580872660
* @version 2.1.7
*/

class UpdateModiulesCloudColumnModuleStatus {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('modules_cloud', 'module_status')) {
			pdo_query("ALTER TABLE " . tablename('modules_cloud') . " ALTER COLUMN `module_status` SET DEFAULT 1;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
