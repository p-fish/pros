<?php

namespace We7\V187;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1544608281
 * @version 1.8.7
 */

class AlterTableUniAccountModulesAddColumnModuleShortcut {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('uni_account_modules', 'module_shortcut')) {
			pdo_query("ALTER TABLE " . tablename('uni_account_modules') . " ADD `module_shortcut` INT(1) NOT NULL DEFAULT 0 COMMENT '插件是否在模块菜单显示，0为不显示，1为显示';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		