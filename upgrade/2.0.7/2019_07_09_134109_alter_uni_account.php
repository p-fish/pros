<?php

namespace We7\V207;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1562650869
 * @version 2.0.7
 */

class AlterUniAccount {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('uni_account', 'createtime')) {
			pdo_query("ALTER TABLE " . tablename('uni_account') . " ADD `createtime` INT(11) NOT NULL DEFAULT 0 COMMENT '创建时间';");
		}
		if (!pdo_fieldexists('uni_account', 'create_uid')) {
			pdo_query("ALTER TABLE " . tablename('uni_account') . " ADD `create_uid` INT(11) NOT NULL DEFAULT 0 COMMENT '创建人uid';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		