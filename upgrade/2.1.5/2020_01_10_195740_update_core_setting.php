<?php

namespace We7\V214;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1578657460
* @version 2.1.4
*/

class UpdateCoreSetting {

	/**
	 *  执行更新
	 */
	public function up() {
		global $_W;
		$settings = $_W['setting']['copyright'];
		if (!empty($settings['icp'])) {
			$icp = array('id' => 1, 'domain' => $_SERVER['HTTP_HOST'], 'icp' => $settings['icp']);
			$icps[] = $icp;
			$settings['icps'] = iserializer($icps);
			unset($settings['icp']);
			setting_save($settings, 'copyright');
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
